import { cloneDeep } from 'lodash';
import gql from 'graphql-tag';

export default {
  name: 'hello',
  apollo: {
    // Simple query that will update the 'author' vue property
    author: {
      query: gql`
      query author($authorId: Int!) {
        author(id: $authorId) {
          id
          firstName
          lastName
        }
      }`,
      variables() {
        return {
          authorId: this.authorId,
        };
      },
      // fetchPolicy: 'cache-and-network',
    },
  },
  data() {
    return {
      msg: 'Welcome to Your Vue.js PWA',
      author: {},
      tmpAuthor: {},
      authorId: 1,
    };
  },
  watch: {
    author() {
      this.tmpAuthor = cloneDeep(this.author);
    },
  },
  methods: {
    changeAuthor() {
      this.authorId = this.authorId === 1 ? 2 : 1;
    },
    changeAuthorName() {
      const newFirstName = this.tmpAuthor.firstName;
      const newLastName = this.tmpAuthor.lastName;
      const authorId = this.authorId;
      const query = gql`
      query author {
        author(id: ${this.authorId}) {
          id
          firstName
          lastName
        }
      }`;
      if (this.author.firstName !== newFirstName) {
        this.$apollo.mutate({
          // Query
          mutation: gql`
            mutation ($authorId: Int!, $newFirstName: String!) {
              changeFirstName(id: $authorId, firstName: $newFirstName) {
                firstName
              }
            }
          `,
          variables: {
            authorId,
            newFirstName,
          },
          update(proxy, mutationResult) {
            const data = proxy.readQuery({ query });
            data.author.firstName = mutationResult.data.changeFirstName.firstName;
            proxy.writeQuery({ query, data });
          },
        });
      }
      if (this.author.lastName !== newLastName) {
        this.$apollo.mutate({
          // Query
          mutation: gql`
            mutation ($authorId: Int!, $newLastName: String!) {
              changeLastName(id: $authorId, lastName: $newLastName) {
                lastName
              }
            }
          `,
          variables: {
            authorId,
            newLastName,
          },
          update(proxy, mutationResult) {
            const data = proxy.readQuery({ query });
            data.author.lastName = mutationResult.data.changeLastName.lastName;
            proxy.writeQuery({ query, data });
          },
        });
      }
    },
  },
};
