import Vue from 'vue';
import Hello from '@/components/Hello';

describe('Hello.vue', () => {
  it('should initialise data()', () => {
    const Constructor = Vue.extend(Hello);
    const vm = new Constructor().$mount();
    expect(vm.$el.querySelector('.hello h1').textContent)
      .to.equal('Welcome to Your Vue.js PWA');
    expect(typeof vm.tmpAuthor).to.not.equal('undefined');
    expect(typeof vm.author).to.not.equal('undefined');
  });

  it('should update authorId', () => {
    const Constructor = Vue.extend(Hello);
    const vm = new Constructor().$mount();
    vm.changeAuthor();
    expect(vm.authorId).to.be.equal(2);
  });

  it('should update author.firstName', () => {
    const Constructor = Vue.extend(Hello);
    const vm = new Constructor().$mount();
    vm.$apollo = {
      mutate(options) {
        vm.author.firstName = options.variables.newName;
      },
    };
    vm.tmpAuthor.firstName = 'test';
    vm.changeAuthorFirstName();
    expect(vm.author.firstName).to.be.equal('test');
  });
});
